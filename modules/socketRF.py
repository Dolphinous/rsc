import time
import platform

if platform.machine() == 'armv7l':  # Kontrola zda-li běží na Raspberry pi
    import pigpio

    pi = pigpio.pi()
    print('SOCKETRF ON NORMAL MODE')
else:  # Pokud ne, přepne se do test modu
    pi = False
    print('SOCKETRF ON TEST MODE')

# Klíč zahajující vysílání
KEY = '11111'

# Kódové sekvence určující stav zásuvky
CODE_ON = '01'
CODE_OFF = '10'

# Přeložení znaků v kódu a klíči na pulsy
KEY_0 = '01'
KEY_1 = '11'
CODE_0 = '10'
CODE_1 = '00'
SEPARATOR = '0'

# Definice délek fází bitů skládajících se z impulsů a pauz
UP_0 = 0.15 / 1000
DOWN_0 = 0.45 / 1000
UP_1 = 0.4 / 1000
DOWN_1 = 0.2 / 1000
TRANSMISSION_PAUSE = 0.5 / 1000

ID_LENGTH = 5  # Množství bitů, jimiž je definované identifikační číslo zásuvky
TRANSMISSION_ATTEMPTS = 10  # Počet opakování vysílání, aby se zajistil příjem

TRANSMIT_PIN = 17  # GPIO hardware číslo pinu, z něhož probíhá vysílání

max_socket_id = 2 ** ID_LENGTH - 1  # Vypočítaný nejvyšší možný identifikační číslo zásuvky na základě počtu dovolených bitů


def format_socket_id(socket_id):  # Převede číslo zásuvky z integeru na bite-code zásuvky
    if socket_id > max_socket_id:  # Podá varování v případě překročení limitu
        print('socket_id %s exceeded the limit of %s' % (socket_id, max_socket_id))

    return format(socket_id, '05b')


def transmit_0():  # Přenos bitu 0
    pi.write(TRANSMIT_PIN, 1)
    time.sleep(UP_0)
    pi.write(TRANSMIT_PIN, 0)
    time.sleep(DOWN_0)


def transmit_1():  # Přenos bitu 1
    pi.write(TRANSMIT_PIN, 1)
    time.sleep(UP_1)
    pi.write(TRANSMIT_PIN, 0)
    time.sleep(DOWN_1)


def socket(socket_id, socket_on, transmission_attempts=10):
    transmission = ''  # Prázdná data k přenosu

    for v in KEY:  # Přeložení klíče do bitů k přenosu
        if v == '0':
            transmission += KEY_0
        elif v == '1':
            transmission += KEY_1

    transmission += SEPARATOR  # Přidání separátoru k přenosu

    socket_code = format_socket_id(socket_id)  # Získání kódu identifkačního čísla žádané zásuvky

    if socket_on:  # Získání kódu žádaného stavu zásuvky
        socket_code += CODE_ON
    else:
        socket_code += CODE_OFF

    for v in socket_code:  # Přeložení kódu identifikačního čísla zásuvky a kódu žádaného stavu zásuvky do bite-codu k přenosu
        if v == '0':
            transmission += CODE_0
        elif v == '1':
            transmission += CODE_1

    if pi:  # V případě že program nevěží v test modu
        for _ in range(transmission_attempts):  # Odvysílání bite-codu
            for v in transmission:
                if v == '0':
                    transmit_0()
                elif v == '1':
                    transmit_1()
            time.sleep(TRANSMISSION_PAUSE)  # Pauza mezi jednotlivými pokusy o přenos
    else:
        print('TEST MODE TRANSMITTING: %s' % transmission)
