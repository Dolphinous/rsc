# Modul udávající strukturu položek v databázi - tzv. modely

from django.db import models


class Socket(models.Model):  # Model zásuvky
    socket_id = models.IntegerField(blank=False)  # Identifikační číslo zásuvky
    state = models.BooleanField(default=False)  # Stav zásuvky (zapnuto/vypnuto)

    def __str__(self):
        return str(self.socket_id)
