# Modul pro nastavení Django Admin rozhraní

from django.contrib import admin
from RSC.models import Socket

# Přidá Model zásuvek do Admin rozhraní
admin.site.register(Socket)
