# -*- coding: utf-8 -*-
# Modul poskytující potřebná nastavení Apachi pro spuštění webové aplikace

import os, sys

sys.path.append('/home/pi/rsc')
sys.path.append('/home/pi/rsc/RSC')
sys.path.append('/home/pi/.local/bin')

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'RSC.settings')

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()