# Modul definující jak mají být zpracovány Http requesty s odpovídajícím URL

from django.contrib import admin
from django.urls import path
from django.contrib.auth.views import LoginView, LogoutView
from RSC import views

urlpatterns = [
    path('', views.home),

    path('admin/', admin.site.urls),
    path('login/', LoginView.as_view(extra_context={'hide_user_bar': True})),
    path('logout/', LogoutView.as_view()),

    path('api/', views.api),
]
