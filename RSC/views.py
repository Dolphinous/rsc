# Hlavní modul v interakci s uživatelem
# Jako vstup bere Http requesty a vrací Http responsy

from modules import socketRF  # Import modulu pro vysílání radiového signálu
from django.shortcuts import render
from RSC.models import Socket
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseNotFound, HttpResponseForbidden
from django.conf import settings


@login_required()  # Tag určující, že k interakci s daným View je nutný být přihlášen
def home(request):  # Základní grafické rozhraní pro interakci se zásuvkami
    template_name = 'home.html'

    sockets = Socket.objects.all()  # Načte z databáze data modelu Socket týkající se zásuvek

    if request.method == 'POST':  # V případě Http POST requestu
        socket_id = int(request.POST.get('socket_id'))
        state = bool(request.POST.get('state'))

        socketRF.socket(socket_id,
                        state)  # Pošle identifikační číslo a stav zásuvky modulu pro vysílání rádiového signálu

        socket = Socket.objects.get(socket_id=socket_id)  # Najde v databázi odpovídající model zásuvky
        socket.state = state  # Změní jeho stav na odpovídající stav
        socket.save()  # Změny v databázi uloží

    # V každém případě se k uživateli pošle Http response s vyrendrovaným Html obsahující aktuální stav zásuvek
    return render(request, template_name, {
        'sockets': sockets,
    })


@csrf_exempt  # Aby API byla pro uživatele co nejjednodušší, jsou vynechána ověření CSRF tokenu
def api(request):
    if request.method == "GET":
        if request.GET.get('api_key') == settings.API_KEY:  # Prosté zabezpečení kontrolující poslaný klíč k API

            if request.GET.get('Lights_on'): # Příkaz pro zapnutí zásuvek 1 a 0
                socketRF.socket(0, True)
                socketRF.socket(1, True)
                return HttpResponse('')

            if request.GET.get('Lights_off'): # Příkaz pro vypnutí zás 1 a 0
                socketRF.socket(0, False)
                socketRF.socket(1, False)
                return HttpResponse('')

        return HttpResponseForbidden()

    return HttpResponseNotFound()
